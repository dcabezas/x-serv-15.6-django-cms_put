from django.http import HttpResponse, HttpResponseNotFound
from django.views.decorators.csrf import csrf_exempt
from .models import Page

html_template = """<!DOCTYPE html>
<html lang="en" >
  <head>
    <meta charset="utf-8" />
    <title>Django CMS</title>
  </head>
  <body>
    {body}
  </body>
</html>
"""
#en el {body} del html_template ira el contenido de cada pagina; si es la principal irá el listado de todas las páginas introducidas y si no es la principal irá el contenido de esa página introducida. 

html_item_template = "<li><a href='{name}'>{name}</a></li>"

def index(request):

    pages = Page.objects.all() #consulto la base de datos para que muestre todas las páginas introducidas hasta la fecha
    if len(pages) == 0: #no hay nada
        body = "No pages yet."
    else: #hay alguna página metida
        body = "<ul>" #lo muestra en formato lista
        for p in pages:
            body += html_item_template.format(name=p.name) #recorro las páginas usando el template, donde le pongo como nombre el nombre de cada página de la base de datos. 
        body += "</ul>"
    return(HttpResponse(html_template.format(body=body))) #relleno el cuerpo de la plantilla en función del contenido.

@csrf_exempt
def page(request, name):

    if request.method == 'PUT':
        try:
            p = Page.objects.get(name=name) #compruebo si hay una página con ese nombre, para no tener dos veces la misma página con el mismo nombre
        except Page.DoesNotExist:
            p = Page(name=name) #creo la página si no estaba ya 
        p.content = request.body.decode("utf-8") #actualizo el contenido bien sea una página nueva o ya introducida y lo convierto a string
        p.save() #guardo en la base de datos lo introducido 

    if request.method == 'GET' or request.method == 'PUT':
        try:
            p = Page.objects.get(name=name) #miro si está la página; la leo si era un GET o un PUT
            content = p.content #me quedo con el contenido 
            response = HttpResponse(content) #con el contenido compongo la respuesta
        except Page.DoesNotExist: #pagina no encontrada el not found es como un 404.ESTE EXCEPT CON PUT NO DEBERÍA OCURRIR NUNCA YA QUE ACABO DE INTRODUCIR UNA PÁGINA; PERO SI ES CON GET SI PODRÍA OCURRIR YA QUE SI HAGO /PACO Y NO ESTÁ INTRODUCIDA CON PUT ESTA PÁGINA NO EXISTIRÁ.
            response = HttpResponseNotFound("Page " + name + " not found")
        return(response) #devuelvo la respuesta
